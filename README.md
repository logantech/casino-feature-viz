# Exploratory data analysis and visualization

* Perform data cleaning, feature imputation, and exploratory data analysis on a time-series dataset with multiple categorical and continuous features.
* Explore features through multiple visualization approaches using Seaborn and Matplotlib.

Quickstart:

* Download Python 3 notebook: LoganDowning-M01-Visualization.ipynb
* Download datasource: JitteredHeadCount.csv
* Open using Jupyter Notebook

Or view this [screen grab](images/LoganDowning-M01-Visualization.png) of the entire notebook.

___

Use exploratory data analysis to reject marginal data.

![Matrix of feature counts over time](images/matrix.png)

Explore data distribution over different time ranges.

![Heatmap of feature values over time](images/heatmap.png)

Identify features that contribute the most to revenue.

![Scatter plot of features showing popularity](images/scatter.png)

Facet feature into bins based on performance to help interpretability.

![Faceting a feature into categories](images/facet.png)

Create a trend analysis over the entire scope of the dataset.

![Trend analysis](images/trend.png)
